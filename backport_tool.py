#!/usr/bin/env python3
# Copyright (c) 2020 freetrader <freetrader@tuta.io>
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

# This is a script to check for new backports from Core and ABC,
# and update a CSV file which we use to track backport planning and status
# in BCHN.

# To use this script, you need to set the *_REPO_PATH variables below which
# point to up-to-date clones of ABC, Core, BCHN and BCHN's public PM repo.

# This script modifies the CSV database in place, but does not commit
# anything. Any changes made by this script can therefore be undone through
# regular (git) version control commands, and should be examined with care
# before committing and pushing them to the published repository, since
# this script is still experimental.

# Semantic rules for backports:
#
# A Merge in Core references the related PR (this can be assumed as a system feature)
# A Diff in ABC can be some self-generated change, or a port of one or more PRs from Core.
# A Diff in ABC can explicitly depend on one or more other Diffs.
# A Diff in ABC may implicitly depend on one or more other Diffs.
# A Diff in ABC may reference one or more Core PRs if it is a backport.
# A Diff in ABC may omit to reference one or more Core PRs if it is a backport. (not sure I know of such cases, but it is possible)

# What would slip through the cracks of this script:
# - Core commits that do not reference any PR. Should not happen! ;-)

import csv
import git
import os
import re
import sys

from enum import Enum, IntEnum, unique

############# BEGIN USER CONFIGURATION SECTION ################
# Values here are freely configurable.

# Repositories which are the data sources:
# Bitcoin Core, Bitcoin ABC, ourselves and our PM repo (for CSV data)
CORE_REPO_PATH = "bitcoin-core"
ABC_REPO_PATH = "bitcoin-abc"
BCHN_REPO_PATH = "bitcoin-cash-node"
PM_REPO_PATH = "bchn-pm-public"

############# END USER CONFIGURATION SECTION ################
# You should not need to modify anything below this line unless you
# are trouble-shooting / developing.

# Relative path of the backports CSV file within our PM repo
CSV_FILE_RELPATH = "planning/bchn-backporting.csv"
DEFAULT_DIALECT = 'excel'

# We do not process commits older than the cutoff which is common in all code repos
CUTOFF='caa2f106d704ec3ade63498031dd58d34510bc76'  # first commit in 2017

# Diff / PR representation pattterns
# used for CSV DB integrity checking
DIFF_PATTERN = re.compile('^D(\d+)')
PR_PATTERN = re.compile('^PR#(\d+)')
DEPENDS_PATTERN = re.compile('^[ ]*Depends on D(\d+)')

# Diff land / PR merge commit patterns
# used to detect which Diffs / PRs are committed in source code repos
ABC_DIFF_LAND_REGEX = re.compile('^Differential Revision: https://reviews.bitcoinabc.org/D(\d+)', re.MULTILINE)
CORE_PR_MERGE_REGEX = re.compile('^Merge\s#(\d+):\s', re.MULTILINE)

# 'SYSTEM' in the comments below indicates that a field is
# populated by the tools and should not be touched manually.
csv_db_field_names = ['bp_id',           # SYSTEM: backport id
                      'bp_status',       # porting status in BCHN
                      'mr',              # MR number (once port in progress)
                      'abc_diff',        # SYSTEM: correlated ABC Diff(s) - can be comma-separated list
                      'core_pr',         # SYSTEM: correlated Core PR(s) - can be comma-separated list
                      'depends',         # comma-separated list of upstream depends (Diffs or PRs)
                      'upstream_commit', # SYSTEM: upstream commit hash of this change
                      'commit_desc',     # SYSTEM: brief description (based on commit title + author)
                      'comment']         # user-defined comment

@unique
class Backport_Status(Enum):
    unevaluated = 1    # no-one has looked deep enough at this to make a call
    deferred = 2       # has been looked at, and decision is to re-evaluate later
    rejected = 3       # this change has been rejected (not needed, or bad, etc)
    planned = 4        # is planned for integration (this DB does not track schedule - that is for taskjuggler to do)
    integrated = 5     # change has been integration, no further action needed at this point
    reverted = 6       # change has been integrated, and since reverted. Should go to deferred or planned if the idea is to re-evaluate or re-integrate later.

@unique
class CSV_Field(IntEnum):
    ''' enum used in indexing '''
    bp_id = 0
    bp_status = 1
    mr = 2
    abc_diff = 3
    core_pr = 4
    depends = 5
    upstream_commit = 6
    commit_desc = 7
    comment = 8


###########################################################################
#  1. read in the CSV database, do some integrity checks
#     if the CSV data is botched, do not continue.

with open(os.path.join(PM_REPO_PATH, CSV_FILE_RELPATH), newline='') as csvfile:
   csv_dict_reader = csv.DictReader(csvfile, dialect=DEFAULT_DIALECT)
   csv_db = [[row['bp_id'],
              row['bp_status'],
              row['mr'],
              row['abc_diff'],
              row['core_pr'],
              row['depends'],
              row['upstream_commit'],
              row['commit_desc'],
              row['comment']] for row in csv_dict_reader]

list_of_bp_ids = [entry[CSV_Field.bp_id] for entry in csv_db]
set_of_bp_ids = set(list_of_bp_ids)

highest_bp_id = 0
if set_of_bp_ids:
   highest_bp_id = max([int(e) for e in list_of_bp_ids])

def fatal_csv_error(msg):
    print("Error: %s" % msg)
    print("Please fix and re-run the program.")
    sys.exit(1)

if len(list_of_bp_ids) > len(set_of_bp_ids):
    fatal_csv_error("there are duplicated backport ids in the CSV data.")

for entry in csv_db:
    entry_id, entry_status, entry_diff, entry_pr = (entry[CSV_Field.bp_id], entry[CSV_Field.bp_status], entry[CSV_Field.abc_diff], entry[CSV_Field.core_pr])
    if entry_status not in [name for name in Backport_Status.__members__]:
       fatal_csv_error("unrecognized backport status '%s' for DB entry %s" % (entry_status, entry_id))
    if entry_status == Backport_Status.integrated.name:
       if not entry_diff and not entry_pr:
           fatal_csv_error("missing PR or Diff info for integrated backport entry %s" % entry_id)

# Sort the CSV data by ascending bp_id.
# We need the correct sort order to later write it out correctly.
sorted_csv_db = sorted(csv_db, key=lambda item: int(item[CSV_Field.bp_id]))
csv_db = sorted_csv_db

###########################################################################
#  2. get list of Diffs and PRs already integrated according to CSV DB

core_repo, abc_repo, bchn_repo = (git.Repo(CORE_REPO_PATH),
                                  git.Repo(ABC_REPO_PATH),
                                  git.Repo(BCHN_REPO_PATH))

core_master, abc_master, bchn_master = [i.heads.master
                                        for i in (core_repo, abc_repo, bchn_repo)]

PRs_already_integrated = []
Diffs_already_integrated = []
PRs_not_evaluated = []
Diffs_not_evaluated = []

def get_depends(line):
   ''' a little parser to extract depends as Dxxx from a line of text'''
   deps = set()
   pos = 0  # position in the string
   end = len(line)
   # skip leading whitespace
   while line[pos] in ' \t':
       pos += 1
   assert('Depends on ' == line[pos:pos+11])
   pos += 11
   while pos < end:
       if (line[pos] == 'D' and line[pos+1] in '0123456789'):
           diff = 'D' + line[pos+1]
           pos += 2
           while pos < end and line[pos] in '0123456789':
               diff += line[pos]
               pos += 1
           deps.add(diff)
           if pos < end:
              if line[pos] in '.':
                 break
       pos += 1
   return sorted(list(deps))

def get_deps_from_ABC_commit_msg(msg):
    ''' returns a list of dependencies extracted from a Diff commit msg'''
    res = []
    split_msg = msg.split('\n')
    for l in split_msg[::-1]:  # iterate over lines in reverse
        if re.search(DEPENDS_PATTERN, l):
            res = get_depends(l)
            if res:
                break
    return res

def is_valid_diff(some_str):
    return re.search(DIFF_PATTERN, some_str)

def is_valid_pr(some_str):
    return re.search(PR_PATTERN, some_str)

for entry in csv_db:
    entry_id, entry_status, entry_diff, entry_pr = (entry[CSV_Field.bp_id], entry[CSV_Field.bp_status], entry[CSV_Field.abc_diff], entry[CSV_Field.core_pr])
    if entry_status == Backport_Status.integrated.name:
       # Diffs
       if ',' in entry_diff:
          split_diffs = entry_diff.split(',')
          for sd in split_diffs:
             if not is_valid_diff(sd):
                fatal_csv_error("'%s' is not a valid pattern in the Diff field of entry %s" % (sd, entry_id))
          Diffs_already_integrated.extend(split_diffs)
       elif entry_diff:
          if not is_valid_diff(entry_diff):
             fatal_csv_error("'%s' is not a valid pattern in the Diff field of entry %s" % (entry_diff, entry_id))
          Diffs_already_integrated.append(entry_diff)
       # PRs
       if ',' in entry_pr:
          split_prs = entry_pr.split(',')
          for spr in split_prs:
             if not is_valid_pr(spr):
                fatal_csv_error("'%s' is not a valid pattern in the PR field of entry %s" % (spr, entry_id))
          PRs_already_integrated.extend(entry_pr.split(','))
       elif entry_pr:
          if not is_valid_pr(entry_pr):
             fatal_csv_error("'%s' is not a valid pattern in the PR field of entry %s" % (entry_pr, entry_id))
          PRs_already_integrated.append(entry_pr)
    elif entry_status == Backport_Status.unevaluated.name:
       # Diffs
       if ',' in entry_diff:
          split_diffs = entry_diff.split(',')
          for sd in split_diffs:
             if not is_valid_diff(sd):
                fatal_csv_error("'%s' is not a valid pattern in the Diff field of entry %s" % (sd, entry_id))
          Diffs_not_evaluated.extend(split_diffs)
       elif entry_diff:
          if not is_valid_diff(entry_diff):
             fatal_csv_error("'%s' is not a valid pattern in the Diff field of entry %s" % (entry_diff, entry_id))
          Diffs_not_evaluated.append(entry_diff)
       # PRs
       if ',' in entry_pr:
          split_prs = entry_pr.split(',')
          for spr in split_prs:
             if not is_valid_pr(spr):
                fatal_csv_error("'%s' is not a valid pattern in the PR field of entry %s" % (spr, entry_id))
          PRs_not_evaluated.extend(entry_pr.split(','))
       elif entry_pr:
          if not is_valid_pr(entry_pr):
             fatal_csv_error("'%s' is not a valid pattern in the PR field of entry %s" % (entry_pr, entry_id))
          PRs_not_evaluated.append(entry_pr)

print("From CSV DB:")
print("   %d entries" % len(csv_db))
print("   %d PRs currently integrated" % len(PRs_already_integrated))
print("   %d Diffs currently integrated" % len(Diffs_already_integrated))
print("   %d PRs currently not evaluated" % len(PRs_not_evaluated))
print("   %d Diffs currently not evaluated" % len(Diffs_not_evaluated))

###########################################################################
#  3. get the list of merged PRs in Core from git

list_of_PRs_merged_in_Core = []
print("Scanning Core repo at %s for PRs merged" % CORE_REPO_PATH)
print("This may take a while...")
# we keep a map to later lookup interesting data for adding to CSV
Core_PR_map = {}

commits_since_cutoff=1
for commit in list(core_repo.iter_commits("master")):
    m = re.search(CORE_PR_MERGE_REGEX, commit.message)
    if m:
        pr_num = m.group(1)
        list_of_PRs_merged_in_Core.append(pr_num)
        Core_PR_map[pr_num] = { "commit": str(commit),
                                "author": commit.author,
                                "message": commit.message } # TODO: date
    if str(commit) == CUTOFF:
        break
    commits_since_cutoff += 1

print("%d PR merges picked up on Core since cutoff" % len(list_of_PRs_merged_in_Core))
print("%d commits on Core since cutoff" % commits_since_cutoff)

###########################################################################
#  4. get the list of merged Diffs in ABC

list_of_Diffs_landed_in_ABC = []
print("Scanning ABC repo at %s for Diffs landed" % ABC_REPO_PATH)
print("This may take a while...")
# we keep a map to later lookup interesting data for adding to CSV
ABC_Diff_map = {}
# and keep a map for any Depends that can be established
ABC_Diff_depends = {}

commits_since_cutoff=1
for commit in list(abc_repo.iter_commits("master")):
    m = re.search(ABC_DIFF_LAND_REGEX, commit.message)
    if m:
        diff_num = m.group(1)
        list_of_Diffs_landed_in_ABC.append(diff_num)
        ABC_Diff_map[diff_num] = { "commit": str(commit),
                                   "author": commit.author,
                                   "message": commit.message } # TODO: date
        deps = get_deps_from_ABC_commit_msg(commit.message)
        if deps:
            ABC_Diff_depends[diff_num] = ','.join(sorted(deps))
        else:
            ABC_Diff_depends[diff_num] = None

    if str(commit) == CUTOFF:
        break
    commits_since_cutoff += 1

print("%d Diff landings picked up on ABC since cutoff" % len(list_of_Diffs_landed_in_ABC))
print("%d commits on ABC since cutoff" % commits_since_cutoff)

###########################################################################
#  5. get the list of merged PRs & landed Diffs in BCHN
#     (ie. the shared history with Core & ABC)

list_of_historic_PRs_ported_into_BCHN = []
list_of_historic_Diffs_ported_into_BCHN = []

print("Scanning BCHN repo at %s for historically shared PRs & Diffs" % BCHN_REPO_PATH)
print("This may take a while...")

commits_since_cutoff=1
for commit in list(bchn_repo.iter_commits("master")):
    m = re.search(ABC_DIFF_LAND_REGEX, commit.message)
    if m:
        diff_num = m.group(1)
        list_of_historic_Diffs_ported_into_BCHN.append(diff_num)
    n = re.search(CORE_PR_MERGE_REGEX, commit.message)
    if n:
        pr_num = n.group(1)
        list_of_historic_PRs_ported_into_BCHN.append(pr_num)
    if str(commit) == CUTOFF:
        break
    commits_since_cutoff += 1

print("%d PR merges and %d Diff landings picked up on BCHN since cutoff" %
      (len(list_of_historic_PRs_ported_into_BCHN),
       len(list_of_historic_Diffs_ported_into_BCHN)))
print("%d commits on BCHN since cutoff" % commits_since_cutoff)

###########################################################################
#  6. for all PRs already in BCHN that are in the Core list:
#     - ensure that this PR is in our main tracking list and its status is Integrated
#     - remove this PR from the list of PRs in Core that we still need to evaluate

def find_bp_id_for_pr(pr_num):
    ''' find a PR's bp_id.
        Returns an integer index >= 0 if found, else -1'''
    for i in range(len(csv_db)):
       # only match PRs if the Diff field is empty and the PR number is the only one in PR field
       if csv_db[i][CSV_Field.abc_diff] == '' and "PR#{}".format(pr_num) == csv_db[i][CSV_Field.core_pr]:
          return i
    return -1   # not found

print("Checking that already-integrated Core PRs have status 'integrated'")
next_bp_id = highest_bp_id + 1

prs_set_to_integrated = 0
set_of_prs_marked_as_integrated = set()
for pr in sorted(list(set(list_of_historic_PRs_ported_into_BCHN) & set(list_of_PRs_merged_in_Core)), key=lambda item: int(item[0]), reverse=False):
   if not "PR#{}".format(pr) in PRs_already_integrated:
      index = find_bp_id_for_pr(pr)
      if index == -1:  # not found, must append
          csv_db.append( (str(next_bp_id), 'integrated', '', "", "PR#%s" % pr, "", "", "", "") )
          next_bp_id += 1
      else:
         csv_db[index][CSV_Field.bp_status] = 'integrated'
      set_of_prs_marked_as_integrated.add(pr)
      prs_set_to_integrated += 1

print("%d PRs marked as integrated" % prs_set_to_integrated)
highest_bp_id = next_bp_id - 1   # set this correctly after the loop

###########################################################################
#  7. for all Diffs already in BCHN that are in the ABC list (how could they not be?):
#     - ensure that this Diff is in our main tracking list and its status is Integrated
#     - remove this Diff from the list of Diffs in ABC that we still need to evaluate

def find_bp_id_for_diff(diff_num):
    ''' find a Diff's bp_id.
        Returns an integer index >= 0 if found, else -1'''
    for i in range(len(csv_db)):
       if "D{}".format(diff_num) == csv_db[i][CSV_Field.abc_diff]:
          return i
    return -1   # not found

print("Checking that already-integrated ABC Diffs have status 'integrated'")
next_bp_id = highest_bp_id + 1
diffs_set_to_integrated = 0
set_of_diffs_marked_as_integrated = set()
for df in sorted(list(set(list_of_historic_Diffs_ported_into_BCHN) & set(list_of_Diffs_landed_in_ABC)), key=lambda item: int(item[0]), reverse=False):
   if not "D{}".format(df) in Diffs_already_integrated:
      index = find_bp_id_for_diff(df)
      if index == -1:  # not found, must append
         csv_db.append( (str(next_bp_id), 'integrated', '', "D%s" % df, "", "", "", "", "") )
         next_bp_id += 1
      else:
         csv_db[index][CSV_Field.bp_status] = 'integrated'
      set_of_diffs_marked_as_integrated.add(df)
      diffs_set_to_integrated += 1

print("%d Diffs marked as integrated" % diffs_set_to_integrated)
highest_bp_id = next_bp_id - 1   # set this correctly after the loop

###########################################################################
#  8. all remaining Core PRs form a list of PRs that are not yet in BCHN

prs_currently_in_db = set([entry[CSV_Field.core_pr] for entry in csv_db if not ',' in entry[CSV_Field.core_pr]])
new_PRs_to_add = sorted(list(set(list_of_PRs_merged_in_Core) - set(list_of_historic_PRs_ported_into_BCHN) - set_of_prs_marked_as_integrated), key=lambda item: int(item), reverse=False)
print("Adding new PRs merged into Core that are not yet in CSV DB:")
next_bp_id = highest_bp_id + 1
prs_to_evaluate = 0

for new_pr in new_PRs_to_add:
    if not "PR#{}".format(new_pr) in PRs_not_evaluated and not "PR#{}".format(new_pr) in prs_currently_in_db:
        core_commit = Core_PR_map[str(new_pr)]["commit"]
        core_author = Core_PR_map[str(new_pr)]["author"]
        core_msg = Core_PR_map[str(new_pr)]["message"].split('\n')[0]
        csv_db.append((str(next_bp_id), 'unevaluated', "", "", "PR#%s" % new_pr, "", str(core_commit), "{} ({})".format(core_msg, core_author), ""))
        prs_to_evaluate += 1
        next_bp_id += 1

print("%d new PRs added as unevaluated" % prs_to_evaluate)
highest_bp_id = next_bp_id - 1   # set this correctly after the loop

###########################################################################
#  9. all remaining ABC Diffs form a list of Diffs that are not yet in BCHN

diffs_currently_in_db = set([entry[CSV_Field.abc_diff] for entry in csv_db])
new_Diffs_to_add = sorted(list(set(list_of_Diffs_landed_in_ABC) - set(list_of_historic_Diffs_ported_into_BCHN) - set_of_diffs_marked_as_integrated), key=lambda item: int(item), reverse=False)
print("Adding new Diffs landed in ABC that are not yet in CSV DB:")
next_bp_id = highest_bp_id + 1
diffs_to_evaluate = 0

for new_df in new_Diffs_to_add:
    if not "D{}".format(new_df) in Diffs_not_evaluated and not "D{}".format(new_df) in diffs_currently_in_db:
        abc_commit = ABC_Diff_map[str(new_df)]["commit"]
        abc_author = ABC_Diff_map[str(new_df)]["author"]
        abc_msg = ABC_Diff_map[str(new_df)]["message"].split('\n')[0]
        csv_db.append((str(next_bp_id), 'unevaluated', "", "D%s" % new_df, "", "", str(abc_commit), "{} ({})".format(abc_msg, abc_author), ""))
        diffs_to_evaluate += 1
        next_bp_id += 1

print("%d new Diffs added as unevaluated" % diffs_to_evaluate)
highest_bp_id = next_bp_id - 1   # set this correctly after the loop

###########################################################################
# 10. update any dependencies

diffs_updated_deps = 0
print("Updating dependencies for ABC Diffs which have some not entered yet")
#print(ABC_Diff_depends.keys())
#sys.exit(1)

for df in ABC_Diff_depends.keys():
   if not "D{}".format(df) in Diffs_already_integrated:
      index = find_bp_id_for_diff(df)
      if index == -1:  # not found, must append
         fatal_csv_error("Unable to find Diff %s in CSV" % df)
      else:
         if ABC_Diff_depends[df] and not csv_db[index][CSV_Field.depends]:
             csv_db[index] = [csv_db[index][CSV_Field.bp_id],
                              csv_db[index][CSV_Field.bp_status],
                              csv_db[index][CSV_Field.mr],
                              csv_db[index][CSV_Field.abc_diff],
                              csv_db[index][CSV_Field.core_pr],
                              ABC_Diff_depends[df],
                              csv_db[index][CSV_Field.upstream_commit],
                              csv_db[index][CSV_Field.commit_desc],
                              csv_db[index][CSV_Field.comment]]
      diffs_updated_deps += 1

print("%d Diffs updated with dependency data" % diffs_updated_deps)

###########################################################################
# 11. write back the CSV DB

with open(os.path.join(PM_REPO_PATH, CSV_FILE_RELPATH), 'w', newline='') as csvfile:
   csv_writer = csv.writer(csvfile, dialect=DEFAULT_DIALECT)
   csv_writer.writerow(csv_db_field_names)
   csv_writer.writerows(sorted(csv_db, key=lambda item: int(item[CSV_Field.bp_id]), reverse=True))

print("Done.")
