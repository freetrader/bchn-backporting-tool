#!/usr/bin/env python3

# Read a dot file given by first argument, and create a HTML page which
# lists backports to work on, grouped into various sections.
# Planned ones with no unmet dependencies can be progressed to MRs and
# the existing MRs will unlock further waiting ones.
# Unevaluated and deferred need further evaluation.
#
# Diffs listed are sorted by ascending Diff number, which should mostly
# correspond to upstream integration order (but not always - see
# 'Limitations' below).
#
# Limitations:
#
# 1. Backports are sorted in the outputs by Diff number, but this may not
#    always be optimal. The order in which they were really landed in ABC
#    codebase would be a better choice, and in future a final pass could
#    be made to adjust them to that order.
#
# 2. Since it will not include nodes that have a mix of integrated and 
#    rejected or deferred, it will not output all that should be considered.
#    The output has been extended to list such problematic cases in a
#    'fall through' list which needs manual examination to determine
#    whether MRs shall be opened.

import sys
import networkx as nx

NUM_COLUMNS = 4  # number of columns to use in generated HTML table

g = nx.drawing.nx_pydot.read_dot(sys.argv[1])

result = []  # for list of nodes which meet criteria
for node in list(g.nodes()):
    if node[0] in ('p', 'u'):  # only consider planned / unevaluated
        all_preds_ok = True
        p_list = list(g.predecessors(node))
        for p in p_list:
            # a node is considered good to include if it has no
            # incident edges (in-degree == 0) OR if all its predecessors
            # have either been integrated or rejected
            if p[0] in ('p', 'd', 'x'):
                # no predecessor may be planned, deferred or reverted
                all_preds_ok = False
                break
        if all_preds_ok:
            result.append(node)

got_mrs = [n[1:] for n in sorted(result) if '!' in n]
unevaluated = [n.split()[0][1:] for n in sorted(result) if n[0] == 'u']
planned = [n.split()[0][1:] for n in sorted(result) if n[0] == 'p' and not n.split()[0][1:] in [g.split()[0] for g in got_mrs]]
deferred = [n.split()[0][1:] for n in sorted(list(g.nodes())) if n[0] == 'd']

node_data = dict(g.nodes.data())
node_data_without_prefix = {}
for n in node_data.keys():
   node_data_without_prefix[n[1:].split(' ')[0]] = node_data[n]


def print_html_table(title_list, item_list):
   print("  <ul class=\"columns\">")
   for t, i in zip(title_list, item_list):
       print("    <li title={0}>{1}</li>".format(t, i))  # t already has quotes
   print("  </ul>")

def phab_urlify(some_diff_list):
   ''' takes a list of Diff in Dxxxx format, and makes them into
       HTML references to Phabricator '''
   phab_list = []
   for d in some_diff_list:
       phab_list.append("<a href=\"https://reviews.bitcoinabc.org/{0}\">{0}</a>".format(d))
   return phab_list

def mr_urlify(some_diff_mr_list):
   ''' takes a list of backports+mr in "Dxxxx (!yyy)" format, and makes
       them into HTML references to GitLab MRs '''
   mr_list = []
   for dm in some_diff_mr_list:
       m = dm.split('!')[1].strip(')')
       mr_list.append("<a href=\"https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/{0}\">{1}</a>".format(m, dm))
   return mr_list

# HTML header
print("<html>")
print("<head></head>")
print("<body>")

print("<h1>Backport planning list</h1>")

print("<p>Notes:</p>")
print("<p>Backport lists are sorted by ascending Diff number.</p>")
print("<p>Read down first column, then second column, etc.</p>")

print("<h2>Existing Diff backport MRs (in progress): {}</h2>".format(len(got_mrs)))
print("<p>These should progress to be integrated (merged) or deferred (if dependency or technical problems detected) or rejected if found unsuitable.</p>")
print_html_table([node_data_without_prefix[d.split(' ')[0]]['tooltip'] for d in got_mrs],
                 mr_urlify(got_mrs))

print("<h2>Diffs already planned: {}</h2>".format(len(planned)))
print("<p>Listed here are only those with no apparent unmet dependencies. Should create MR, resolve conflicts and attempt to integrate.</p>")
print_html_table([node_data_without_prefix[d.split(' ')[0]]['tooltip'] for d in planned],
                 phab_urlify(planned))

print("<h2>Diffs to be evaluated: {}</h2>".format(len(unevaluated)))
print("<p>These need to be looked at and classified either as planned, deferred or rejected.</p>")
print_html_table([node_data_without_prefix[d.split(' ')[0]]['tooltip'] for d in unevaluated],
                 phab_urlify(unevaluated))

print("<h2>Diffs deferred: {}</h2>".format(len(deferred)))
print("<p>These have been previously classified as not able to proceed - need further evaluation and become either planned, or rejected.</p>")
print_html_table([node_data_without_prefix[d.split(' ')[0]]['tooltip'] for d in deferred],
                 phab_urlify(deferred))

listed_so_far = set(got_mrs) | set(unevaluated) | set(planned) | set(deferred)
fell_through = []
for n in sorted(list(g.nodes())):
    if n[0] in ('p', 'd') and n[1:] not in listed_so_far:
       fell_through.append(n[1:])
print("<h3>Diff backports waiting on predecessor integration (or somehow fell through the cracks): {}</h3>".format(len(fell_through)))
print("<p>Most of these just have dependencies on others that are not yet integrated.</p>")
print_html_table([node_data_without_prefix[d.split(' ')[0]]['tooltip'] for d in fell_through],
                 phab_urlify(fell_through))

print("</body>")

print("<style type=\"text/css\">ul {{ columns: {0}; -webkit-columns: {0}; -moz-columns: {0}; }}</style>".format(NUM_COLUMNS))
