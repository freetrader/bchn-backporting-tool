#!/usr/bin/env python3
# Copyright (c) 2020 freetrader <freetrader@tuta.io>
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

# This is a script to create a dependency graph in graphviz .dot format
# from the CSV database of backports that have not been completed/rejected.
# At this stage it is a quick hack based on backport_tool.py.
# Both scripts need to be refactored to use a better internal representation
# of the DB.
#
# Post-processing suggestions:
#
# To get ASCII representation of graph using Perl-based 'graph-easy',
# pipe the output through: 'graph-easy --from=dot --as_ascii'.
# This is easy to search in when you using a terminal.
#
# SVG output which shows the commit description tooltips:
# filter through 'dot -Tsvg'

import csv
import os
import sys
import html

from enum import Enum, IntEnum, unique

############# BEGIN USER CONFIGURATION SECTION ################
# Values here are freely configurable.

# Repositories which are the data sources:
# Bitcoin Core, Bitcoin ABC, ourselves and our PM repo (for CSV data)
CORE_REPO_PATH = "bitcoin-core"
ABC_REPO_PATH = "bitcoin-abc"
BCHN_REPO_PATH = "bitcoin-cash-node"
PM_REPO_PATH = "bchn-pm-public"

############# END USER CONFIGURATION SECTION ################
# You should not need to modify anything below this line unless you
# are trouble-shooting / developing.

# Relative path of the backports CSV file within our PM repo
CSV_FILE_RELPATH = "planning/bchn-backporting.csv"
DEFAULT_DIALECT = 'excel'

# 'SYSTEM' in the comments below indicates that a field is
# populated by the tools and should not be touched manually.
csv_db_field_names = ['bp_id',           # SYSTEM: backport id
                      'bp_status',       # porting status in BCHN
                      'mr',              # MR number (once port in progress)
                      'abc_diff',        # SYSTEM: correlated ABC Diff(s) - can be comma-separated list
                      'core_pr',         # SYSTEM: correlated Core PR(s) - can be comma-separated list
                      'depends',         # comma-separated list of upstream depends (Diffs or PRs)
                      'upstream_commit', # SYSTEM: upstream commit hash of this change
                      'commit_desc',     # SYSTEM: brief description (based on commit title + author)
                      'comment']         # user-defined comment

@unique
class Backport_Status(Enum):
    unevaluated = 1    # no-one has looked deep enough at this to make a call
    deferred = 2       # has been looked at, and decision is to re-evaluate later
    rejected = 3       # this change has been rejected (not needed, or bad, etc)
    planned = 4        # is planned for integration (this DB does not track schedule - that is for taskjuggler to do)
    integrated = 5     # change has been integration, no further action needed at this point
    reverted = 6       # change has been integrated, and since reverted. Should go to deferred or planned if the idea is to re-evaluate or re-integrate later.

@unique
class CSV_Field(IntEnum):
    ''' enum used in indexing '''
    bp_id = 0
    bp_status = 1
    mr = 2
    abc_diff = 3
    core_pr = 4
    depends = 5
    upstream_commit = 6
    commit_desc = 7
    comment = 8


###########################################################################
#  1. read in the CSV database, do some integrity checks
#     if the CSV data is botched, do not continue.

with open(os.path.join(PM_REPO_PATH, CSV_FILE_RELPATH), newline='') as csvfile:
   csv_dict_reader = csv.DictReader(csvfile, dialect=DEFAULT_DIALECT)
   csv_db = [[row['bp_id'],
              row['bp_status'],
              row['mr'],
              row['abc_diff'],
              row['core_pr'],
              row['depends'],
              row['upstream_commit'],
              row['commit_desc'],
              row['comment']] for row in csv_dict_reader]

list_of_bp_ids = [entry[CSV_Field.bp_id] for entry in csv_db]
set_of_bp_ids = set(list_of_bp_ids)

highest_bp_id = 0
if set_of_bp_ids:
   highest_bp_id = max([int(e) for e in list_of_bp_ids])

def fatal_csv_error(msg):
    print("Error: %s" % msg)
    print("Please fix and re-run the program.")
    sys.exit(1)

if len(list_of_bp_ids) > len(set_of_bp_ids):
    fatal_csv_error("there are duplicated backport ids in the CSV data.")

for entry in csv_db:
    entry_id, entry_status, entry_diff, entry_pr = (entry[CSV_Field.bp_id], entry[CSV_Field.bp_status], entry[CSV_Field.abc_diff], entry[CSV_Field.core_pr])
    if entry_status not in [name for name in Backport_Status.__members__]:
       fatal_csv_error("unrecognized backport status '%s' for DB entry %s" % (entry_status, entry_id))
    if entry_status == Backport_Status.integrated.name:
       if not entry_diff and not entry_pr:
           fatal_csv_error("missing PR or Diff info for integrated backport entry %s" % entry_id)

# Sort the CSV data by ascending bp_id.
# We need the correct sort order to later write it out correctly.
sorted_csv_db = sorted(csv_db, key=lambda item: int(item[CSV_Field.bp_id]))
csv_db = sorted_csv_db

###########################################################################
#  2. write dotfile header

print("digraph g{ node [shape=\"box\",style=rounded]; rankdir=\"LR\";\n")


###########################################################################
#  3. write dotfile body (the dependency data)

STATUS_SYMBOLS = { Backport_Status.unevaluated.name: 'u',
                   Backport_Status.planned.name: 'p',
                   Backport_Status.rejected.name: 'r',
                   Backport_Status.deferred.name: 'd',
                   Backport_Status.integrated.name: 'i',
                   Backport_Status.reverted.name: 'x' }

STATUS_COLORS = { 'u': 'grey',
                  'p': 'green',
                  'r': 'red',
                  'd': 'blue',
                  'i': 'black',
                  'x': 'brown' }

# get a map of status symbols
diff_status_map = {}
commit_msg_map = {}
for entry in csv_db:
    entry_status, entry_diff, entry_commit_msg = (entry[CSV_Field.bp_status], entry[CSV_Field.abc_diff], entry[CSV_Field.commit_desc])
    diff_status_map[entry_diff] = STATUS_SYMBOLS[entry_status]
    commit_msg_map[entry_diff] = html.escape(entry_commit_msg)

# compile node definition list
node_lines = []
diff_mr_map = {}
decoration = {}
href = {}
for entry in csv_db:
    entry_status, entry_diff, entry_mr = (entry[CSV_Field.bp_status], entry[CSV_Field.abc_diff], entry[CSV_Field.mr])
    if entry_diff:
        decorated_entry_diff = diff_status_map[entry_diff] + entry_diff
        diff_mr_map[entry_diff] = entry_mr
        node_lines.append(decorated_entry_diff)
        if diff_mr_map[entry_diff]: # if it has an MR, use MR url
            decoration[entry_diff] = ' (' + diff_mr_map[entry_diff] + ')'
            href[entry_diff] = "https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/{}".format(diff_mr_map[entry_diff][1:])
        else:  # use Phabricator Diff url
            decoration[entry_diff] = ''
            href[entry_diff] = "https://reviews.bitcoinabc.org/{}".format(entry_diff)

# compile edges list
edge_lines = []
for entry in csv_db:
    entry_id, entry_status, entry_diff, entry_depends = (entry[CSV_Field.bp_id], entry[CSV_Field.bp_status], entry[CSV_Field.abc_diff], entry[CSV_Field.depends])
    if entry_status == Backport_Status.unevaluated.name or entry_status == Backport_Status.planned.name or entry_status == Backport_Status.deferred.name:
       # Diffs
       if ',' in entry_diff:
           fatal_csv_error("Multiple Diff numbers in Diff field of entry id %s" % entry_id)
       decorated_entry_diff = diff_status_map[entry_diff] + entry_diff
       if entry_depends:
           for dep_diff in entry_depends.split(','):
              if dep_diff.startswith('PR'):
                 # skip any PRs in deps for now
                 pass
              else:
                 decorated_dep_diff = diff_status_map[dep_diff] + dep_diff
                 edge_lines.append([decorated_dep_diff, decorated_entry_diff])

# write out only the nodes that are part of an edge
vertices_used =  set([edge[0] for edge in edge_lines]) | set([edge[1] for edge in edge_lines])
for l in node_lines:
   decorated_l = l + decoration[l[1:]]
   if l in vertices_used:  # node is part of a dependency edge
      print("   \"{0}\" [color={1} fontcolor={1} tooltip=\"{2}\" href=\"{3}\"]".format(decorated_l, STATUS_COLORS[l[0]], commit_msg_map[l[1:]], href[l[1:]]))
   else: # isolated node
      if diff_status_map[l[1:]] in ('p', 'd', 'r', 'u'):
         # TODO : group them according to status?
         print("   \"{0}\" [color={1} fontcolor={1} tooltip=\"{2}\" href=\"{3}\"]".format(decorated_l, STATUS_COLORS[l[0]], commit_msg_map[l[1:]], href[l[1:]]))

# write out the edges
for e in edge_lines:
   print("   \"{}\" -> \"{}\"".format(e[0] + decoration[e[0][1:]], e[1] + decoration[e[1][1:]]))

###########################################################################
#  4. write dotfile footer

print("}")
