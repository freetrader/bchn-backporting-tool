# freetrader's tools for tracking backporting on BCHN

So far there are three scripts:

1. `backport_tool.py`, which is used to maintain `planning/bchn-backporting.csv` in the public BCHN PM repository. This script is not run automatically by CI yet - it is run manually, and the output inspected and revised with manual planning decisions.
2. the `create_backporting_dependency_dot_file.py` script for creating a 'dot' graph file based on the CSV file. This is an aid in planning changes to status of various ports, and is run by CI on public PM repo to generate https://schedule.bitcoincashnode.org/bchn-backport-diffs-graph.dot from the CSV database contents. The dot file is also used to generate https://schedule.bitcoincashnode.org/bchn-backport-diffs-graph.svg .
3. the `suggest_backport_ordering.py` script to generate tables of backports in various stages, to ease work planning. The tables list the backports (currently only Diffs) in ascending order so that implicit dependencies can be avoided as much as possible during integration.

The rest of this README will focus on instructions related to the
`backport_tool.py` (referred to as 'the tool' below).
In the future when more tools are added, this documentation here
might be moved to a separate document.

These instructions are only for installation / usage of the tool,
not about how to evaluate or further process BCHN backports.
Information about that should be found in maintainer documentation
within the Bitcoin Cash Node project.


### Requirements

- Python3 - the script has only been lightly tested on some systems
  with Python 3.5.2 or higher

- git-python (`pip3 install gitpython`)

- graphviz (for the dot graph generation tool)

- networkx (for html planning table script)

- pydot (for html planning table script)


### Usage

Unpack or clone this repository to some directory.

Create the required input text files (see next section on data format).

Run the script:

    $ ./backport_tool.py

The tool will produce some output informing about number of backports
(Diffs and Pull Requests) it picks up from the configured repository
sources.

It will modify the CSV file in the PM repository, you can then inspect
the changes and push them to a merge request against the public repository
when satisfied.

The tool will do some validation that the CSV file has not been corrupted,
but in some cases corruption may go undetected. Careful examination of
the changes made to existing data in the file should be done before
committing changes to the CSV output.


### Inputs

(This is also described in the header of the backport_tool.py script)

There are four input folders expected at paths set by configuration
contained in the tool script. Their names are configured by default to
be:

* bitcoin-core
* bitcoin-abc
* bitcoin-cash-node
* bchn-pm-public

If you have existing clones of these repositories, you can create
symbolic links to them within the working directory of the tool.
Alternatively you can edit the USER CONFIGURATION SECTION at the top of the
script which is marked by BEGIN / END comments.

The first three folder should contain up-to-date source code of Bitcoin Core,
Bitcoin ABC and Bitcoin Cash Node (BCHN) in their `master` branches.

Be sure to fetch the latest changes for `master` branches of each before
running the tool.

The fourth folder should contain an up-to-date view of the BCHN PM
repository which contains the CSV file which is both an input and output
of the tool.

### Output

There is some output on stdout about what the tool is doing, and if there
are errors there will be error messages.

Otherwise, the tool should finish with a "Done." output indicating all
steps were performed with nominal success, and it has written any
changes (if necessary) to the CSV file (`planning/bchn-backporting.csv`
in the PM repository).

### Caveats / limitations

This tool has only been tested on Debian Linux (several releases).
It is still very much in development, and should be considered alpha
software at this stage.

Note that if you have made changes of your own in data fields of the
CSV file, the tool _should_ preserve those changes during normal operation.
But to be on the safe side, it is best to commit your own changes before
running the tool.

If you make manual changes to fields inside the CSV file, please take note
of the following:

- field delimiter is a comma
- the CSV dialect read/written by the tool is Excel
- no quoting of fields needed unless the fields contain comma delimiters. If
  you add a field value which contains commas (e.g. a comma-separated list
  of "depends" Diffs or a long comment field with a comma in it) then
  please add surrounding double quotes around that field value.

As long as the above are observed, if the tool wipes out some uncommitted
changes on that file, the author would like to know about it because it is
probably a bug.


### MIT License

The tools in this repository are made available under the MIT license.
See the top level COPYING file.
